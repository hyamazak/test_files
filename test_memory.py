"""
Tools for creating vertical slice models.
"""
from firedrake import *
parameters['assembly_cache']['enabled'] = False

periodic = True

L = 200000.
H = 100000.
nlayers = 10
ncolumns = 300

## Setting a mesh
if periodic:
    m = CircleManifoldMesh(ncolumns)
    coord_fs = VectorFunctionSpace(m, 'DG', 1, dim=1)
    old_coordinates = Function(m.coordinates)
    new_coordinates = Function(coord_fs)

    periodic_kernel = """double Y,pi;
    Y = 0.5*(old_coords[0][1]-old_coords[1][1]);
    pi=3.141592653589793;
    for(int i=0;i<2;i++){
    new_coords[i][0] = atan2(old_coords[i][1],old_coords[i][0])/pi/2;
    if(new_coords[i][0]<0.) new_coords[i][0] += 1;
    if(new_coords[i][0]==0 && Y<0.) new_coords[i][0] = 1.0;
    new_coords[i][0] *= L;
    }"""

    periodic_kernel = periodic_kernel.replace('L',str(L))

    par_loop(periodic_kernel, dx,
                     {"new_coords":(new_coordinates,WRITE),
                      "old_coords":(old_coordinates,READ)})

    m.coordinates = new_coordinates
else:
    m = IntervalMesh(ncolumns, L)

mesh = ExtrudedMesh(m, layers=nlayers, layer_height=H/nlayers)

##Setting up the finite element spaces
#1D spaces in horizontal
IUh0 = FiniteElement("CG", "interval", 2)
IUh1 = FiniteElement("DG", "interval", 1)

#1D spaces in vertical
IUv0 = FiniteElement("CG", "interval", 2)
IUv1 = FiniteElement("DG", "interval", 1)

# Pressure space
V2_elt = OuterProductElement(IUh1, IUv1)
V2 = FunctionSpace(mesh, V2_elt)

# Velocity space
V1_v = OuterProductElement(IUh1, IUv0)
V1_h = OuterProductElement(IUh0, IUv1)
V1_elt = HDiv(V1_v) + HDiv(V1_h)
V1 = FunctionSpace(mesh,V1_elt)

# Temperature space
Vt = FunctionSpace(mesh,V1_v)

# Mixed function space storing all of the velocity field
W = MixedFunctionSpace((V1,V2,Vt,V2))

## Solution variables
# values of u,v,p,b at current timestep
class old(object): pass
old = old()
old.vars = Function(W)
oldu, oldv, oldb, oldp = old.vars.split()

# values of u,v,p,b at next timestep
class new(object): pass
new = new()
new.vars = Function(W)
newu, newv, newb, newp = new.vars.split()

# iterative increments for newvel, newp, newb
Deltavars = Function(W)
Deltau, Deltav, Deltab, Deltap = Deltavars.split()        

## Initial setting
# set old.vars
b0_exp = Expression("0.01*sin(pi*x[1]/100000.)/(1+pow(x[0]-0.5*200000.,2)/pow(10000.,2))")
oldu.assign(project(Expression(("0.0","0.0")),oldu.function_space()))
oldv.assign(project(Expression("0.0"),oldv.function_space()))
oldb.assign(project(b0_exp,oldb.function_space()))
oldp.assign(project(Expression("0.0"),oldp.function_space()))
        
# set new.vars
new.vars.assign(old.vars)

# set Deltavars
b1_exp = Expression("0.001*sin(pi*x[1]/100000.)/(1+pow(x[0]-0.5*200000.,2)/pow(10000.,2))")
Deltau.assign(project(Expression(("0.0","0.0")),Deltau.function_space()))
Deltav.assign(project(Expression("0.0"),Deltav.function_space()))
Deltab.assign(project(b1_exp,Deltab.function_space()))
Deltap.assign(project(Expression("0.0"),Deltap.function_space()))

# open file 
file_b = File("temp_b.pvd")
file_b << oldb

## time integration
print "Starting the time loop!"
for nstep in range(100000):

    new.vars += Deltavars
    old.vars.assign(new.vars)

    # file_b << oldb
    # print nstep


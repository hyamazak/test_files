"""
Tools for creating vertical slice models.
"""
from firedrake import *
from utility_functions import make_fbar
from configuration import Configuration
import sys
import numpy as np
op2.init(log_level='WARNING')
parameters['assembly_cache']['enabled'] = False
from firedrake.petsc import PETSc

class uvbpPC(object):
    """
    Class for implementing preconditioner for coupled u,v,b,p system
    with preconditioner that does (approximate) elimination.
    """
    def setup(self,W,V1,V2,Vt,dt,alpha,Nsq,csq,f,ksp_atol,ksp_rtol):
        self.w_in = Function(W)
        self.w_out = Function(W)

        u_in,v_in,b_in,p_in = self.w_in.split()
        u_out,v_out,b_out,p_out = self.w_out.split()

        #decoupled system for v
        v = TrialFunction(V2)
        phi = TestFunction(V2)
        self.vmass = assemble(v*phi*dx)

        #Eliminating b
        #First we need to turn residual into a function by dividing
        #by a b mass matrix
        r = TestFunction(Vt)
        b = TrialFunction(Vt)
        self.bmass = assemble(r*b*dx)
        #the solution goes in bRes
        self.bRes = Function(Vt)

        #Function space for u,p
        V = MixedFunctionSpace((V1,V2))
        u,p = TrialFunctions(V)
        w,q = TestFunctions(V)

        #eliminating b from u,p equation
        b = -dt*alpha*Nsq*u[1] - self.bRes

        a = (
            inner(w,u) + dt*alpha*( - div(w)*p - w[1]*b)+
            q*(p + dt*alpha*csq*div(u))
            )*dx
        self.A = assemble(lhs(a))
        #need to delay assembly of RHS as bRes changes
        self.aRHS = rhs(a)

        bc1 = [DirichletBC(V[0], Expression(("0." , "0.")), x)
                    for x in ["top", "bottom"]]
        for bc in bc1:
            bc.apply(self.A)

        #Function to solve u,p equation into
        self.up_out = Function(V)
        u,p = self.up_out.split()

        #RHS for reassembling b
        r = TestFunction(Vt)
        b = TrialFunction(Vt)
        self.bL = r*(-self.bRes-dt*alpha*Nsq*u[1])*dx

        # solvers
        self.bsolver = LinearSolver(self.bmass,
                                    solver_parameters={'ksp_type':'preonly',
                                                       'pc_type':'bjacobi',
                                                       'sub_pc_type':'ilu'})
        solver_parameters={'ksp_type':'preonly',
                           'pc_type': 'fieldsplit',
                           'pc_fieldsplit_type': 'schur',
                           'pc_fieldsplit_schur_fact_type': 'full',
                           'pc_fieldsplit_schur_precondition': 'selfp',
                           'fieldsplit_1_ksp_type': 'preonly',
                           'fieldsplit_1_pc_type': 'gamg',
                           'fieldsplit_1_mg_levels_pc_type': 'bjacobi',
                           'fieldsplit_1_mg_levels_sub_pc_type': 'ilu',
                           'fieldsplit_0_ksp_type': 'richardson',
                           'fieldsplit_0_ksp_max_it': 4,
                           'ksp_atol': ksp_atol,
                           'ksp_rtol': ksp_rtol}

        self.upsolver = LinearSolver(self.A,
                              solver_parameters=solver_parameters)

        self.vsolver = LinearSolver(self.vmass,
                                    solver_parameters={'ksp_type':'preonly',
                                                       'pc_type':'bjacobi',
                                                       'sub_pc_type':'ilu'})


    def apply(self, pc, x, y):
        # y = PC*x
        #Get the input vector into a w_in, a Firedrake function
        with self.w_in.dat.vec as w_in:
            # No need to copy if w_in as actually x
            if w_in != x:
                w_in.array[:] = x.array[:]

        u_in,v_in,b_in,p_in = self.w_in.split()
        u_out,v_out,b_out,p_out = self.w_out.split()

        self.bsolver.solve(self.bRes,b_in)
        up_rhs = assemble(self.aRHS)
        u_rhs, p_rhs = up_rhs.split()
        u_rhs += u_in
        p_rhs += p_in

        # LA solve is solve(A, x, b)
        self.upsolver.solve(self.up_out,up_rhs)
        u_out1, p_out1 = self.up_out.split()
        u_out.assign(u_out1)
        p_out.assign(p_out1)

        self.vsolver.solve(v_out,v_in)

        bLHS = assemble(self.bL)
        self.bsolver.solve(b_out,bLHS)

        #Copy from w_out into y
        # Note vec_ro in this case (you're not writing into the
        # firedrake function)
        with self.w_out.dat.vec_ro as v:
            # No need to copy out if w_out is y
            if v != y:
                y.array[:] = v.array[:]


class SliceConfiguration(Configuration):

    """
    Setup input for slice models
    """
    fields = ['u','v','b','p']

    def __init__(self, **kwargs):
        self.L = None
        self.H = None
        self.nlayers = None
        self.ncolumns = None
        self.pvertical = 2
        self.phorizontal = 2
        self.dt = None
        self.t = 0.
        self.end = None
        self.Tdump = None
        self.dstep = 1
        self.output_fields = []
        self.output_fields_FS = {}
        self.nits = 4
        self.alpha = 0.5
        self.Nsq = Constant(0)
        self.csq = 300.**2
        self.f = Constant(0)
        self.mu = Constant(0)
        self.mu_exp = None
        self.initial_conditions = {}
        self.topo_exp = None
        self.periodic = True
        self.ksp_atol = 1.e-04
        self.ksp_rtol = 1.e-04
        self.ksp_monitor = False
        self.snes_monitor = False
        self.ksp_monitor_true_residual = True
        self.filename = None

        Configuration.__init__(self, **kwargs)

        self.initial_conditions.setdefault('u', Expression(("0.0","0.0")))
        for field in self.fields:
            self.initial_conditions.setdefault(field, Expression("0.0"))

class FieldAdvection(object):

    """ A base class for advecting fields.

    Takes arguments: p: a class which should contain the variable dt
                     oldf: the function for the field at time level n
                     newf: the function for the field at time level n+1
                     old: class containing fields at time level n
                     new: class containing fields at time level n+1

    Sets up test, trial and residual functions in the same function 
    space as the function oldf.

    Defines get_residual method, which in the linear case is simply
    solver.solve()

    Subclasses are required for each field to define the setup_solver.

    """

    def __init__(self, p, oldf, newf, old, new):

        self.p = p
        self.oldf = oldf
        self.newf = newf
        self.old = old
        self.new = new

        self.function_space = oldf.function_space()
        self.test = TestFunction(self.function_space)
        self.trial = TrialFunction(self.function_space)
        self.residual = Function(self.function_space)

    def get_residual(self):

        self.solver.solve()

class BAdvection(FieldAdvection):

    def setup_solver(self):

        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        bmass = self.test*self.trial*dx
        Lbres = (
            self.test*(self.newf-self.oldf + self.p.dt*self.p.Nsq*ubar[1])
            )*dx

        bresproblem = LinearVariationalProblem(bmass,Lbres,self.residual)
        self.solver = LinearVariationalSolver(bresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

class PAdvection(FieldAdvection):

    def setup_solver(self):

        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        pmass = self.test*self.trial*dx
        Lpres = (
            self.test*(self.newf-self.oldf + self.p.dt*self.p.csq*div(ubar))
            )*dx

        presproblem = LinearVariationalProblem(pmass,Lpres,self.residual)
        self.solver = LinearVariationalSolver(presproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})
class VAdvection(FieldAdvection):

    def __init__(self, p, oldf, newf, old, new, uinit):

        FieldAdvection.__init__(self, p, oldf, newf, old, new)
        self.uinit = uinit

    def setup_solver(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu

        vmass = self.test*self.trial*dx
        Lvres = (self.test*(self.newf-self.oldf) + 
                 self.p.dt*self.p.f*self.test*(ubar[0]-self.uinit[0]))*dx

        vresproblem = LinearVariationalProblem(vmass, Lvres, self.residual)
        self.solver = LinearVariationalSolver(vresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

class UAdvection(FieldAdvection):

    def __init__(self, p, oldf, newf, old, new, uinit,
                 no_normal_flow_bc_ids):

        FieldAdvection.__init__(self, p, oldf, newf, old, new)
        self.uinit = uinit
        self.no_normal_flow_bc_ids = no_normal_flow_bc_ids

    def setup_solver(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        newu, newv, newb, newp = self.new.vars.split()
        ubar = (1 - self.p.alpha)*oldu + self.p.alpha*newu
        vbar = (1 - self.p.alpha)*oldv + self.p.alpha*newv
        bbar = (1 - self.p.alpha)*oldb + self.p.alpha*newb
        pbar = (1 - self.p.alpha)*oldp + self.p.alpha*newp

        test = self.test
        dt = self.p.dt

        umass = inner(test,self.trial)*dx
        Lures = (inner(test,self.newf - self.oldf)
                 - dt*self.p.f*test[0]*vbar
                 - dt*(div(test)*pbar + test[1]*bbar)
                 - dt*test[1]*self.p.mu*ubar[1])*dx
        bc1 = [DirichletBC(test.function_space(), Expression(("0.","0.")), 
                           bdry_id) for bdry_id in self.no_normal_flow_bc_ids]
        uresproblem = LinearVariationalProblem(umass, Lures, 
                                               self.residual, bcs=bc1)
        self.solver = LinearVariationalSolver(uresproblem, 
                                              solver_parameters=
                                              {'ksp_type': 'cg'})

class LinearSlice():
    """
    A base class for slice models, with
    residuals for the linear equations.

    setup should be a configuration object 
    containing arguments that differ from 
    the defaults set in SliceConfiguration
    """

    def __init__(self, setup):

        self.parameters = setup
        p = self.parameters

        # open files
        self.files_projected = {}
        self.files_unprojected = {}

        for field in p.output_fields_FS:
            self.files_projected[field] = File(
                p.filename+'_'+field+'_'+p.output_fields_FS[field]+'.pvd')

        for field in p.output_fields:
            self.files_unprojected[field] = File(p.filename+'_'+field+'.pvd')

        # get number of time steps
        self.nsteps = int(p.end/p.dt)

        self.no_normal_flow_bc_ids=["top", "bottom"]
        if p.periodic:
            # Setting up a periodic mesh suitable for parallelization
            m = CircleManifoldMesh(p.ncolumns)
            coord_fs = VectorFunctionSpace(m, 'DG', 1, dim=1)
            old_coordinates = Function(m.coordinates)
            new_coordinates = Function(coord_fs)

            periodic_kernel = """double Y,pi;
            Y = 0.5*(old_coords[0][1]-old_coords[1][1]);
            pi=3.141592653589793;
            for(int i=0;i<2;i++){
            new_coords[i][0] = atan2(old_coords[i][1],old_coords[i][0])/pi/2;
            if(new_coords[i][0]<0.) new_coords[i][0] += 1;
            if(new_coords[i][0]==0 && Y<0.) new_coords[i][0] = 1.0;
            new_coords[i][0] *= L;
            }"""

            periodic_kernel = periodic_kernel.replace('L',str(p.L))

            par_loop(periodic_kernel, dx,
                     {"new_coords":(new_coordinates,WRITE),
                      "old_coords":(old_coordinates,READ)})

            m.coordinates = new_coordinates
        else:
            m = IntervalMesh(p.ncolumns, p.L)
            self.no_normal_flow_bc_ids.append(1)
            self.no_normal_flow_bc_ids.append(2)

        mesh = ExtrudedMesh(m, layers=p.nlayers, layer_height=p.H/p.nlayers)
        self.mesh = mesh

        #1D spaces in horizontal
        IUh0 = FiniteElement("CG", "interval", p.phorizontal)
        IUh1 = FiniteElement("DG", "interval", p.phorizontal-1)
        self.IUh0 = IUh0

        #1D spaces in vertical
        IUv0 = FiniteElement("CG", "interval", p.pvertical)
        IUv1 = FiniteElement("DG", "interval", p.pvertical-1)
        self.IUv0 = IUv0

        ##Setting up the finite element spaces

        # Pressure space
        V2_elt = OuterProductElement(IUh1, IUv1)
        V2 = FunctionSpace(self.mesh, V2_elt)

        # Velocity space
        self.V1_v = OuterProductElement(IUh1, IUv0)
        self.V1_h = OuterProductElement(IUh0, IUv1)
        V1_elt = HDiv(self.V1_v) + HDiv(self.V1_h)
        V1 = FunctionSpace(self.mesh,V1_elt)

        # Vertical part of velocity space
        V1v_elt = HDiv(self.V1_v)
        V1v = FunctionSpace(self.mesh,V1v_elt)
        self.V1v = V1v

        # Temperature space
        Vt = FunctionSpace(self.mesh,self.V1_v)

        # Mixed function space storing all of the velocity field
        W = MixedFunctionSpace((V1,V2,Vt,V2))
        self.W = W

        # Solution variables
        class old(object): pass
        self.old = old()

        old.vars = Function(W)

        # sponge layer setting
        if p.mu_exp is not None:
            Vmu = FunctionSpace(mesh, "DG", 1)
            p.mu = Function(Vmu).project(p.mu_exp)

        # values of u,v,p,b at next timestep
        class new(object): pass
        self.new = new()
        new.vars = Function(W)
        newu, newv, newb, newp = new.vars.split()

        # iterative increments for newvel, newp, newb
        self.Deltavars = Function(W)

        # setup diagnostics
        self.setup_diagnostics()

        self.field_dict = {name: func for (name, func) in 
                           zip(("u", "v", "b", "p"), old.vars.split())}
        self.field_dict["w"] = old.vars.split()[0][1]

        # initial conditions
        oldu, oldv, oldb, oldp = old.vars.split()
        for field in p.fields:
            if isinstance(p.initial_conditions[field], expression.Expression):
                self.field_dict[field].project(p.initial_conditions[field])

        # store initial u
        self.uinit = project(p.initial_conditions["u"],V1)

        # set topography
        if p.topo_exp is not None:
            X = self.mesh.coordinates
            X.project(p.topo_exp)

        self.field_advection = {
            "u" : UAdvection(p, oldu, newu, old, new, self.uinit,
                             self.no_normal_flow_bc_ids),
            "v" : VAdvection(p, oldv, newv, old, new, self.uinit),
            "b" : BAdvection(p, oldb, newb, old, new),
            "p" : PAdvection(p, oldp, newp, old, new),
            }

        self.ures = self.field_advection["u"].residual
        self.vres = self.field_advection["v"].residual
        self.bres = self.field_advection["b"].residual
        self.pres = self.field_advection["p"].residual

    def hydrostatic_initial_pressure(self):

        # get functions
        oldu, oldv, oldb, oldp = self.old.vars.split()

        ##Calculate hydrostatic pressure
        # get F
        v = TrialFunction(self.V1v)
        w = TestFunction(self.V1v)

        bcs = [DirichletBC(self.V1v, Expression(("0.", "0.")), "bottom")]

        a = inner(w,v)*dx
        L = w[1]*oldb*dx
        F = Function(self.V1v)

        solve(a == L, F, bcs=bcs)

        # define mixed function space
        V2 = oldp.function_space()
        WV = (self.V1v)*(V2)

        # get pprime
        v,pprime = TrialFunctions(WV)
        w,phi = TestFunctions(WV)

        bcs = [DirichletBC(WV[0], Expression(("0.", "0.")), "bottom")]

        a = (
            inner(w,v) + div(w)*pprime + div(v)*phi
            )*dx
        L = phi*div(F)*dx
        w1 = Function(WV)

        solver_parameters={'ksp_type':'gmres',
                           'pc_type': 'fieldsplit',
                           'pc_fieldsplit_type': 'schur',
                           'pc_fieldsplit_schur_fact_type': 'full',
                           'pc_fieldsplit_schur_precondition': 'selfp',
                           'fieldsplit_1_ksp_type': 'preonly',
                           'fieldsplit_1_pc_type': 'gamg',
                           'fieldsplit_1_mg_levels_pc_type': 'bjacobi',
                           'fieldsplit_1_mg_levels_sub_pc_type': 'ilu',
                           'fieldsplit_0_ksp_type': 'richardson',
                           'fieldsplit_0_ksp_max_it': 4,
                           'ksp_atol': 1.e-08,
                           'ksp_rtol': 1.e-08}

        solve(a == L, w1, bcs=bcs,
              solver_parameters=solver_parameters)

        v,pprime = w1.split()
        oldp.assign(project(pprime,V2))

    def pressure_boundary_correction(self):
        # get parameters
        parameters = self.parameters

        # get functions
        oldu, oldv, oldb, oldp = self.old.vars.split()

        ##Boundary correction
        # define mixed function space
        Vt = oldb.function_space()
        V2 = oldp.function_space()
        WT = (Vt)*(V2)

        #get phat
        ptrial,q = TrialFunctions(WT)
        gamma,phi = TestFunctions(WT)

        bcs = [DirichletBC(WT[0], Expression("0."), "bottom")]

        a = (gamma*ptrial-gamma.dx(1)*q+phi*ptrial.dx(1))*dx
        L = phi*oldp*dx

        F = Function(WT)

        solver_parameters={'ksp_type':'gmres',
                           'pc_type': 'fieldsplit',
                           'pc_fieldsplit_type': 'schur',
                           'pc_fieldsplit_schur_fact_type': 'full',
                           'pc_fieldsplit_schur_precondition': 'selfp',
                           'fieldsplit_1_ksp_type': 'preonly',
                           'fieldsplit_1_pc_type': 'gamg',
                           'fieldsplit_1_mg_levels_pc_type': 'bjacobi',
                           'fieldsplit_1_mg_levels_sub_pc_type': 'ilu',
                           'fieldsplit_0_ksp_type': 'richardson',
                           'fieldsplit_0_ksp_max_it': 4,
                           'ksp_atol': 1.e-08,
                           'ksp_rtol': 1.e-08}

        solve(a == L, F, bcs=bcs,
              solver_parameters=solver_parameters)

        phat,q = F.split()

        # define mixed function space
        WV = (self.V1v)*(V2)

        # get pbar
        v,pbar = TrialFunctions(WV)
        w,phi = TestFunctions(WV)
        
        bcs = [DirichletBC(WV[0], Expression(("0.","0.")), "bottom")]

        a = (
            inner(w,v) + div(w)*pbar + div(v)*phi
            )*dx
        L = w[1]*phat*ds_t
        w1 = Function(WV)

        solve(a == L, w1, bcs=bcs,
              solver_parameters=solver_parameters)

        v,pbar = w1.split()

        # get corrected pressure
        oldp.assign(project(oldp,V2) - project(pbar,V2)/parameters.H) 


    def geostrophic_initial_velocity(self):
        # get parameters
        p = self.parameters

        # get functions
        oldu, oldv, oldb, oldp = self.old.vars.split()

        ##Calculate blanced velocity
        # get pressure gradient
        V1 = oldu.function_space()
        g = TrialFunction(V1)
        wg = TestFunction(V1)

        n = FacetNormal(self.mesh)

        a = inner(wg,g)*dx
        L = -div(wg)*oldp*dx + inner(wg,n)*oldp*ds_tb
        pgrad = Function(V1)
        solve(a == L, pgrad)

        # get oldv
        V2 = oldv.function_space()
        phi = TestFunction(V2)
        m = TrialFunction(V2)

        a = p.f*phi*m*dx
        L = phi*pgrad[0]*dx
        solve(a == L, oldv)


    def setup_duvbp_solver(self):
        #THIS IS ALWAYS THE SAME
        # get parameters
        p = self.parameters
        # get space
        W = self.W
        # trial and test functions
        du, dv, db, dp = TrialFunctions(W)
        w, wv, gamma, phi = TestFunctions(W)
        
        Equ = (
            inner(w,du) 
            - p.alpha*p.dt*p.f*w[0]*dv - p.alpha*p.dt*div(w)*dp
            - p.alpha*p.dt*w[1]*(db - p.mu*du[1])
            + inner(w,self.ures)
            + wv*dv + p.alpha*p.dt*p.f*wv*du[0] + inner(wv,self.vres)
            + gamma*(db + p.alpha*p.dt*p.Nsq*du[1]) + gamma*self.bres
            + phi*(dp + p.alpha*p.dt*p.csq*div(du)) + phi*self.pres
            )*dx
        A = lhs(Equ)
        L = rhs(Equ)

        # Boundary conditions
        bc1 = [DirichletBC(W[0], Expression(("0." , "0.")), x)
               for x in ["top", "bottom"]]
        Duvbpproblem = LinearVariationalProblem(A,L,self.Deltavars,bcs=bc1)

        pc = uvbpPC()
        
        pc.setup(W,self.ures.function_space(),self.pres.function_space(),self.bres.function_space(),p.dt,p.alpha,p.Nsq,p.csq,p.f,p.ksp_atol,p.ksp_rtol)
        sp = {'pc_type': 'python', 
              'ksp_type':'gmres',
              'ksp_monitor': p.ksp_monitor,
              'snes_monitor': p.snes_monitor,
              'ksp_monitor_true_residual': p.ksp_monitor_true_residual}
        self.Duvbpsolver = LinearVariationalSolver(Duvbpproblem,
                                                   solver_parameters=sp)
        self.Duvbpsolver.snes.ksp.pc.setPythonContext(pc)

    def diagnostic_u_residual(self):
        ures = np.abs(self.ures.dat.data).max()
        return ures

    def diagnostic_v_residual(self):
        vres = np.abs(self.vres.dat.data).max()
        return vres

    def diagnostic_b_residual(self):
        bres = np.abs(self.bres.dat.data).max()
        return bres

    def diagnostic_p_residual(self):
        pres = np.abs(self.pres.dat.data).max()
        return pres

    def diagnostic_kinetic_energy(self):
        oldu, oldv, oldb, oldp = self.old.vars.split()
        kinetic = assemble(0.5*(dot(oldu,oldu)+oldv*oldv)*dx)
        return kinetic

    def diagnostic_potential_energy(self):
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        potential = assemble(0.5*dot(oldb,oldb)/p.Nsq*dx)
        return potential

    def diagnostic_internal_energy(self):
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        internal = assemble(0.5*dot(oldp,oldp)/p.csq*dx)
        return internal

    def diagnostic_total_energy(self):
        total = (
            self.diagnostic_kinetic_energy()
            +self.diagnostic_potential_energy()
            +self.diagnostic_internal_energy()
            )
        return total

    def diagnostic_courantnumber_x(self):
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        input_u = oldu[0]
        outspace = oldp.function_space()
        output_u = Function(outspace)
        prsolver_u = self.setup_projection_solver(input_u, output_u, outspace)
        prsolver_u.solve()
        umax = np.abs(output_u.dat.data).max()
        courant_x = umax*p.dt/(p.L/float(p.ncolumns))
        return courant_x

    def diagnostic_courantnumber_z(self):
        p = self.parameters
        oldu, oldv, oldb, oldp = self.old.vars.split()
        input_w = oldu[1]
        outspace = oldp.function_space()
        output_w = Function(outspace)
        prsolver_w = self.setup_projection_solver(input_w, output_w, outspace)
        prsolver_w.solve()
        wmax = np.abs(output_w.dat.data).max()
        courant_z = wmax*p.dt/(p.H/float(p.nlayers))
        return courant_z

    def setup_diagnostics(self):
        self.DiagList = ["diagnostic_u_residual", "diagnostic_v_residual", 
                         "diagnostic_b_residual", "diagnostic_p_residual",
                         "diagnostic_kinetic_energy", 
                         "diagnostic_potential_energy", 
                         "diagnostic_internal_energy", 
                         "diagnostic_total_energy",
                         "diagnostic_courantnumber_x", 
                         "diagnostic_courantnumber_z"]

        # set list of list
        self.Data = []
        for i in range(len(self.DiagList)):
            self.Data.append([])

    def get_diagnostics(self):
        for i in range(len(self.Data)):
            fn = getattr(self,self.DiagList[i])
            self.Data[i].append(fn())

    def setup_projection_solver(self,input,output,outspace):
        # trial and test functions
        tri = TrialFunction(outspace)
        tes = TestFunction(outspace)

        # setup a solver to project input onto outspace
        ax = inner(tes,tri)*dx
        Lx = inner(tes,input)*dx
        prproblem = LinearVariationalProblem(ax, Lx, output)

        prsolver = LinearVariationalSolver(prproblem, 
                                           solver_parameters={'ksp_type': 'cg'})
        return prsolver

    def setup_project_result(self):
        # get parameters
        p = self.parameters

         # set projecting solvers
        self.output = {}
        outspace = {}
        self.prsolver = {}

        for field in self.files_projected:
            outspace[field] = getattr(self,p.output_fields_FS[field])
            self.output[field] = Function(outspace[field])
            self.prsolver[field] = (
                self.setup_projection_solver(self.field_dict[field],
                                             self.output[field],
                                             outspace[field])
                )

    def dump(self):
        # dump flow results
        for field in self.files_projected:
            self.prsolver[field].solve()
            self.files_projected[field] << self.output[field]

        for field in self.files_unprojected:
            self.files_unprojected[field] << self.field_dict[field]

    @op2.MPI.rank_zero
    def mpiprint(self,str):
        print str

    def initialise(self):
        # not needed here but provides possibility to do 
        # e.g. balanced initialisation for the Eady problem
        pass

    def run(self):        
        # get parameters
        p = self.parameters

        # initialisation
        self.initialise()

        # setup residuals
        # Subclasses may redefine these functions
        self.field_advection["b"].setup_solver()
        self.field_advection["p"].setup_solver()
        self.field_advection["u"].setup_solver()
        self.field_advection["v"].setup_solver()

        # setup solvers, these are always the same
        self.setup_duvbp_solver()

        # set newvel,newp,newb
        self.new.vars.assign(self.old.vars)

        # setup prsolvers
        self.setup_project_result()

        # dump intial flow results and diagnostics
        oldu, oldv, oldb, oldp = self.old.vars.split()
        file_b = File("temp_b.pvd")
        file_b << oldb

        # set Deltavars
        for its in range(p.nits):
            self.field_advection["b"].get_residual()
            self.field_advection["p"].get_residual()
            self.field_advection["u"].get_residual()
            self.field_advection["v"].get_residual()
    
        self.Duvbpsolver.solve()

        # time loop
        self.mpiprint("Starting the time loop!")
        
        for nstep in range(100000):

            self.new.vars += self.Deltavars
            self.old.vars.assign(self.new.vars)

            file_b << oldb

            print nstep




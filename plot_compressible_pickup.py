from pylab import *
import json
import matplotlib.pyplot as plt

## IMPORT DATA ##
f = open('compressible_eady/diagnostics.json', 'r')
data = json.load(f)

N = len(data["u"]["rms"])
DATE = []
for i in range(N):
    DATE.append(i*2./24.)

RMSV = data["MeridionalVelocity"]["rms"]
Vmax = data["MeridionalVelocity"]["max"]
TotalEnergy = data["CompressibleEadyTotalEnergy"]["total"]
Potential = data["CompressibleEadyPotentialEnergy"]["total"]
KineticTotal = data["CompressibleKineticEnergy"]["total"]
KineticV = data["CompressibleKineticEnergyV"]["total"]

## APPEND PICKUP DATA ##
f = open('compressible_eady_pickup/diagnostics.json', 'r')
data = json.load(f)

NP = len(data["u"]["rms"])
for i in range(NP):
    DATE.append((i+N)*2./24.)

RMSV.extend(data["MeridionalVelocity"]["rms"])
Vmax.extend(data["MeridionalVelocity"]["max"])
TotalEnergy.extend(data["CompressibleEadyTotalEnergy"]["total"])
Potential.extend(data["CompressibleEadyPotentialEnergy"]["total"])
KineticTotal.extend(data["CompressibleKineticEnergy"]["total"])
KineticV.extend(data["CompressibleKineticEnergyV"]["total"])

KineticUW = []
for i in range(N+NP):
    KineticUW.append(KineticTotal[i] - KineticV[i])
TotalDiff = []
for i in range(N+NP):
    TotalDiff.append(TotalEnergy[i] - TotalEnergy[0])
PotentialDiff = []
for i in range(N+NP):
    PotentialDiff.append(Potential[i] - Potential[0])
KineticVDiff = []
for i in range(N+NP):
    KineticVDiff.append(KineticV[i] - KineticV[0])
KineticUWDiff = []
for i in range(N+NP):
    KineticUWDiff.append(KineticUW[i] - KineticUW[0])

## PLOT RMSV ##
plt.figure(num=1,figsize=(9,6))

plt.plot(DATE, RMSV, label = r'RMS', 
         color = 'black', linestyle='-', linewidth=1.5)
plt.plot(DATE, Vmax, label = r'Max', 
         color = 'black', linestyle='-.', linewidth=1.5)

xlabel(r't (days)', fontsize=18)
ylabel(r'$v$ (ms$^{-1}$)', fontsize=18)

plt.title("Out-of-slice velocity", fontsize=20)
plt.legend(loc='upper left', prop={'size':12})
plt.grid(which='major',color='gray',linestyle='-')
plt.xlim(xmin=0.)
plt.savefig('rmsv.eps')

## PLOT KINETIC ENERGY ##
plt.figure(num=2,figsize=(9,6))

plt.plot(DATE, KineticUW, label = r'KineticUW', 
         color = 'black', linestyle=':', linewidth=1.5)
plt.plot(DATE, KineticV, label = r'KineticV', 
         color = 'black', linestyle='-', linewidth=1.5)
plt.plot(DATE, KineticTotal, label = r'KineticTotal', 
         color = 'red', linestyle='-', linewidth=1.5)

xlabel(r't (days)', fontsize=18)
ylabel(r'Energy', fontsize=18)

plt.title("Kinetic energy", fontsize=20)
plt.legend(loc='upper left', prop={'size':12})
plt.grid(which='major',color='gray',linestyle='-')
plt.xlim(xmin=0.)
plt.savefig('kinetic_energy.eps')

## PLOT ENERGY ##
plt.figure(num=3,figsize=(9,6))

plt.plot(DATE, TotalEnergy, label = r'Total', 
         color = 'red', linestyle='-', linewidth=1.5)
plt.plot(DATE, Potential, label = r'Potential', 
         color = 'black', linestyle='--', linewidth=1.5)
plt.plot(DATE, KineticV, label = r'KineticV', 
         color = 'black', linestyle='-', linewidth=1.5)
plt.plot(DATE, KineticUW, label = r'KineticUW', 
         color = 'black', linestyle=':', linewidth=1.5)

xlabel(r't (days)', fontsize=18)
ylabel(r'Energy', fontsize=18)

plt.title("Energy", fontsize=20)
plt.legend(loc='upper left', prop={'size':12})
plt.grid(which='major',color='gray',linestyle='-')
plt.xlim(xmin=0.)
plt.savefig('energy.eps')

## PLOT ENERGY DIFFERENCES ##
plt.figure(num=4,figsize=(9,6))

plt.plot(DATE, TotalDiff, label = r'TotalDiff', 
         color = 'red', linestyle='-', linewidth=1.5)
plt.plot(DATE, PotentialDiff, label = r'PotentialDiff', 
         color = 'black', linestyle='--', linewidth=1.5)
plt.plot(DATE, KineticVDiff, label = r'KineticVDiff', 
         color = 'black', linestyle='-', linewidth=1.5)
plt.plot(DATE, KineticUWDiff, label = r'KineticUWDiff', 
         color = 'black', linestyle=':', linewidth=1.5)

xlabel(r't (days)', fontsize=18)
ylabel(r'Energy', fontsize=18)

plt.title("Energy differences from the initial state", fontsize=20)
plt.legend(loc='upper left', prop={'size':12})
plt.grid(which='major',color='gray',linestyle='-')
plt.xlim(xmin=0.)
plt.savefig('energy_diff.eps')

## DISPLAY PLOTS ##
plt.show()

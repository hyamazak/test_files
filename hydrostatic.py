from firedrake import *
import inifns

L = 1000000.
H = 10000.
Nsq = 2.5e-05
dbdy = -10./300.*3.0e-06
a = -7.5
Bu = 0.5
f = 1.0e-4
nlayers = 50
dz = H/nlayers

m = PeriodicIntervalMesh(nlayers, length=2*L)
mesh = ExtrudedMesh(m, layers=nlayers, layer_height=dz)

#1D spaces in horizontal
IUh0 = FiniteElement("CG", "interval", 1)
IUh1 = FiniteElement("DG", "interval", 0)

#1D spaces in vertical
IUv0 = FiniteElement("CG", "interval", 1)
IUv1 = FiniteElement("DG", "interval", 0)

##Setting up the finite element spaces

#Pressure space
V2_elt = OuterProductElement(IUh1, IUv1)
V2 = FunctionSpace(mesh, V2_elt)

#Velocity space
V1_v = OuterProductElement(IUh1, IUv0)
V1_h = OuterProductElement(IUh0, IUv1)
V1_elt = HDiv(V1_v) + HDiv(V1_h)
V1 = FunctionSpace(mesh,V1_elt)

#Vertical part of velocity space
V1v_elt = HDiv(V1_v)
V1v = FunctionSpace(mesh,V1v_elt)

#Vortiity space
V0_elt = OuterProductElement(IUh0, IUv0)
V0 = FunctionSpace(mesh, V0_elt)

#Temperature space
Vt = FunctionSpace(mesh,V1_v)

#Mixed function spaces
W = V1*V2
WV = V1v*V2

#Solution variables
template_s = inifns.template_target_strings()
b0_exp = Expression(template_s,a=a,Nsq=Nsq,Bu=Bu,H=H,L=L)
oldb = Function(Vt)
oldb.project(b0_exp)


###################################
# CALCULATE HYDROSTATIC PRESSURE 
###################################

#Get F
v = TrialFunction(V1v)
w = TestFunction(V1v)

#Boundary conditions
bcs = [DirichletBC(V1v, Expression(("0.", "0.")), "bottom")]

a = inner(w,v)*dx
L = w[1]*oldb*dx

F = Function(V1v)
solve(a == L, F, bcs=bcs)


#Get hydrostatic pressure
v,pprime = TrialFunctions(WV)
w,phi = TestFunctions(WV)

#Boundary conditions
bcs = [DirichletBC(WV[0], Expression(("0.", "0.")), "bottom")]

a = (
    inner(w,v) + div(w)*pprime + div(v)*phi
    )*dx

L = phi*div(F)*dx

w1 = Function(WV)

solve(a == L, w1, bcs=bcs)

v,pprime = w1.split()


###################################
# CALCULATE HYDROSTATIC PRESSURE WITH BOUNDARY CORRECTION
###################################

#Get F
v = TrialFunction(V1v)
w = TestFunction(V1v)

a = inner(w,v)*dx
L = w[1]*pprime*dx

F = Function(V1v)
solve(a == L, F)


#Get phat
v,phat = TrialFunctions(WV)
w,phi = TestFunctions(WV)

#Boundary conditions
bcs = [DirichletBC(WV[0], Expression(("0.", "0.")), "bottom")]

a = (
    inner(w,v) + div(w)*phat + div(v)*phi
    )*dx

L = phi*div(F)*dx

w1 = Function(WV)

solve(a == L, w1, bcs=bcs)

v,phat = w1.split()


#Get pbar
v,pbar = TrialFunctions(WV)
w,phi = TestFunctions(WV)

#Boundary conditions
bcs = [DirichletBC(WV[0], Expression(("0.","0.")), "bottom")]

a = (
    inner(w,v) + div(w)*pbar + div(v)*phi
    )*dx

L = w[1]*phat*ds_t

w1 = Function(WV)

solve(a == L, w1, bcs=bcs)

v,pbar = w1.split()


#Get corrected pressure
oldp = pprime - pbar/H


###################################
# CALCULATE MERIDIONAL VELOCITY 
###################################

#Get pressure gradient 
g = TrialFunction(V1)
wg = TestFunction(V1)

#Boundary conditions
bcs = [DirichletBC(V1, Expression(("0." , "0.")), x)
       for x in ["top", "bottom"]]

a = inner(wg,g)*dx
L = -div(wg)*pprime*dx

pgrad = Function(V1)
solve(a == L, pgrad, bcs=bcs)


#Get oldv
phi = TestFunction(V2)
m = TrialFunction(V2)

a = f*phi*m*dx
L = phi*pgrad[0]*dx

oldv = Function(V2)

solve(a == L, oldv)


###################################
# CALCULATE VELOCITY VECTOR
###################################

#Solve the blanced equation
#psi is a stream function
xsi = TestFunction(V0)
psi = TrialFunction(V0)

Forcing = Function(V0).interpolate(Expression(("x[1]-H/2"),H=H))

Equ = (
    xsi.dx(0)*Nsq*psi.dx(0) + xsi.dx(1)*f**2*psi.dx(1)
    + dbdy*xsi.dx(0)*oldv - dbdy*xsi.dx(1)*f*Forcing
    )*dx
Au = lhs(Equ)
Lu = rhs(Equ)

stm = Function(V0)

solve(Au == Lu, stm)

#Get velocity components from the stream function
zu = -stm.dx(1)
zw = stm.dx(0)

#Assign velocity variables
oldu = Function(V1)
oldu.assign = zu, zw

oldvel = Function(W)
oldvel.assign = oldu, oldv


###################################
# OUTPUT IN V0 SPACE
###################################

#setup solver for projecting result
def SetupPrSolver(input,output,outspace):
    # trial and test functions
    tri = TrialFunction(outspace)
    tes = TestFunction(outspace)
    
    # setup a solver to project input onto outspace
    ax = inner(tes,tri)*dx
    Lx = inner(tes,input)*dx
    prproblem = LinearVariationalProblem(ax, Lx, output)

    prsolver = LinearVariationalSolver(prproblem, parameters={'ksp_type': 'cg'})
    return prsolver

#open files
fileb = File("hydro_b.pvd")
filep = File("hydro_p.pvd")
filev = File("hydro_v.pvd")
fileu = File("hydro_u.pvd")
filew = File("hydro_w.pvd")

filepprime = File("hydro_pprime.pvd")
filephat = File("hydro_phat.pvd")
filepbar = File("hydro_pbar.pvd")

#set function space to project results on
outspace = V0

#output b
output = Function(outspace)
prsolver = SetupPrSolver(oldb,output,outspace)
prsolver.solve()
fileb << output
prsolver.destroy()

#output p
output = Function(outspace)
prsolver = SetupPrSolver(oldp,output,outspace)
prsolver.solve()
filep << output
prsolver.destroy()

#output meridional velocity
output = Function(outspace)
prsolver = SetupPrSolver(oldv,output,outspace)
prsolver.solve()
filev << output
prsolver.destroy()

#output horizontal velocity
output = Function(outspace)
prsolver = SetupPrSolver(zu,output,outspace)
prsolver.solve()
fileu << output
prsolver.destroy()

#output vertical velocity
output = Function(outspace)
prsolver = SetupPrSolver(zw,output,outspace)
prsolver.solve()
filew << output
prsolver.destroy()

#output pprime
output = Function(outspace)
prsolver = SetupPrSolver(pprime,output,outspace)
prsolver.solve()
filepprime << output
prsolver.destroy()

#output phat
output = Function(outspace)
prsolver = SetupPrSolver(phat,output,outspace)
prsolver.solve()
filephat << output
prsolver.destroy()

#output pbar
output = Function(outspace)
prsolver = SetupPrSolver(pbar,output,outspace)
prsolver.solve()
filepbar << output
prsolver.destroy()


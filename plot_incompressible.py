from pylab import *
import json
import matplotlib.pyplot as plt

## IMPORT DATA ##
f = open('diagnostics.json', 'r')
data = json.load(f)

DATE = []
for i in range(len(data["MeridionalVelocity"]["rms"])):
    DATE.append(i*2./24.)

## PLOT RMSV ##
plt.figure(num=1,figsize=(9,6))

plt.plot(DATE, data["MeridionalVelocity"]["rms"], 
         label = r'RMS', color = 'black', 
         linestyle='-', linewidth=1.5)

plt.legend(loc='upper left', prop={'size':12})

xlabel(r't (days)', fontsize=18)
ylabel(r'$v$ (ms$^{-1}$)', fontsize=18)

plt.grid(which='major',color='gray',linestyle='-')

plt.xlim(xmax=25.)
#plt.ylim(ymax=100.)

plt.savefig('rmsv.eps')


## PLOT Energy ##
plt.figure(num=2,figsize=(9,6))

KineticUW = []
for i in range(len(data["MeridionalVelocity"]["rms"])):
    KineticUW.append(data["KineticEnergy"]["total"][i]
                     - data["KineticEnergyV"]["total"][i])

plt.plot(DATE, KineticUW, 
         label = r'KineticUW', color = 'black', 
         linestyle=':', linewidth=1.5)
plt.plot(DATE, data["KineticEnergyV"]["total"], 
         label = r'KineticV', color = 'black', 
         linestyle='-', linewidth=1.5)
plt.plot(DATE, data["EadyPotentialEnergy"]["total"], 
         label = r'Potential', color = 'black', 
         linestyle='--', linewidth=1.5)
plt.plot(DATE, data["EadyTotalEnergy"]["total"], 
         label = r'Total', color = 'red', 
         linestyle='--', linewidth=1.5)

plt.legend(loc='upper left', prop={'size':12})

xlabel(r't (days)', fontsize=18)
ylabel(r'$v$ (ms$^{-1}$)', fontsize=18)

plt.grid(which='major',color='gray',linestyle='-')

plt.xlim(xmax=25.)

plt.savefig('energy.eps')

plt.show()

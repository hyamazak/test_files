from slicemodels import SliceConfiguration, LinearSlice
from firedrake import *

b0_exp = Expression("0.01*sin(pi*x[1]/100000.)/(1+pow(x[0]-0.5*200000.,2)/pow(10000.,2))")

dt = 100.

setup = SliceConfiguration(
    L=200000.,H=100000.,nlayers=10,ncolumns=300,
    dt=dt, end=3000.,Tdump=1*dt,
    output_fields = ['b'],
    initial_conditions = {'b': b0_exp}, 
    Nsq=1.e-4, f=1.e-4, nits=2, 
    filename='test_linear')

Model = LinearSlice(setup)

Model.run()

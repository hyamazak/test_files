from pylab import *
import json
import matplotlib.pyplot as plt

## IMPORT DATA ##
with open('diagnostics.json', 'r') as f:
    obj = json.load(f)
    RMSV = obj["VerticalVelocity"]["rms"]

time = []
for i in range(len(RMSV)):
    time.append(200*(i-1))

plt.figure(num=None,figsize=(9,6))

plt.plot(time, RMSV, label = r'RMS', color = 'black', linestyle='-', linewidth=1.5)

plt.legend(loc='upper left', prop={'size':12})

xlabel(r't (days)', fontsize=18)
ylabel(r'$v$ (ms$^{-1}$)', fontsize=18)

plt.grid(which='major',color='gray',linestyle='-')

plt.xlim(xmin=0.)
#plt.xlim(xmax=25.)
#plt.ylim(ymax=100.)

#plt.savefig('../figures/Figure4.eps')

plt.show()
